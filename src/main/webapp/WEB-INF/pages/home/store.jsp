<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!-- section start -->
<section class="section-b-space ratio_asos">
    <div class="collection-wrapper">
        <div class="container">
            <div class="row">
                <div class="col-sm-3 collection-filter sidebar">
                    <!-- side-bar colleps block stat -->
                    <div class="collection-filter-block">
                        <!-- brand filter start -->
                        <div class="collection-mobile-back"><span class="filter-back"><i class="fa fa-angle-left" aria-hidden="true"></i> </span></div>
                        <div class="collection-collapse-block open">
                            <h3 class="collapse-block-title">Nos Catégorie</h3>
                            <ul class="list-group list-group-flush">
                                <c:forEach var="category" items="${categories}">
                                    <li class="list-group-item "><a href="<c:url value='/store/${category.name.toLowerCase()}'/>">${category.name}</a></li>
                                </c:forEach> 
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="collection-content col">
                    <div class="page-main-content">
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="top-banner-wrapper">
                                    <a href="#">
                                        <img src="../ressources/images/pexels-photo-1030845coper.jpg" class="img-fluid blur-up lazyload" alt="">
                                    </a>
                                </div>
                                <div class="collection-product-wrapper">
                                    <div class="product-top-filter">
                                        <div class="row">
                                            <div class="col-xl-12">
                                                <div class="filter-main-btn"><span class="filter-btn btn btn-theme"><i class="fa fa-filter" aria-hidden="true"></i> Filtrer</span></div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-12">
                                                <div class="product-filter-content">

                                                    <div class="collection-view">
                                                        <ul>
                                                            <li><i class="fa fa-th grid-layout-view"></i></li>
                                                            <li><i class="fa fa-list-ul list-layout-view"></i></li>
                                                        </ul>
                                                    </div>
                                                    <div class="collection-grid-view">
                                                        <ul>
                                                            <li><img src="<c:url value='/resources/images/icon/2.png'/>" alt="" class="product-2-layout-view"></li>
                                                            <li><img src="<c:url value='/resources/images/icon/3.png'/>" alt="" class="product-3-layout-view"></li>
                                                            <li><img src="<c:url value='/resources/images/icon/4.png'/>" alt="" class="product-4-layout-view"></li>
                                                            <li><img src="<c:url value='/resources/images/icon/6.png'/>" alt="" class="product-6-layout-view"></li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="product-wrapper-grid">
                                        <div class="row">
                                            <c:forEach var="product" items="${products}">
                                                <c:set var="image" value="${product.getMasterImage()}"/>
                                                <c:if test="${image != null}">
                                                <div class="col-xl-3 col-md-6 col-grid-box">
                                                    <div class="product-box">
                                                        <div class="img-wrapper">
                                                            <div class="front">
                                                                <a href="<c:url value='/store/${product.getProductLink()}'/>">
                                                                     <img src="http://old.belarome.ma/uploads/product_images/${image.id}/thumb250_${image.url}"
                                                                                 class="img-fluid blur-up lazyload bg-img-product" alt="">
                                                                </a>
                                                            </div>
                                                            <div class="back">
                                                                <a href="<c:url value='/store/${product.getProductLink()}'/>">
                                                                <img src="http://old.belarome.ma/uploads/product_images/${image.id}/thumb250_${image.url}"
                                                                            class="img-fluid blur-up lazyload bg-img-product"  alt="">
                                                            </div>
                                                            <div class="cart-info cart-wrap">
                                                                <button data-toggle="modal" data-target="#addtocart"  title="Add to cart"><i class="ti-shopping-cart" ></i></button>
                                                                <a href="javascript:void(0)" title="Add to Wishlist"><i class="ti-heart" aria-hidden="true"></i></a>
                                                                <a href="#" data-toggle="modal" data-target="#quick-view" title="Quick View"><i class="ti-search" aria-hidden="true"></i></a>
                                                            </div>
                                                        </div>
                                                        <div class="product-detail">
                                                            <div>
                                                                <a href="<c:url value='/store/${product.getProductLink()}'/>"><h6>${product.name}</h6></a>
                                                                <p>Discription</p>
                                                                <h4>${product.retailPrice} DH</h4>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                </c:if>
                                                <!-- construct an "update" link with customer id -->
                                                <c:url var="updateLink" value="/products/create">
                                                    <c:param name="productId" value="${product.id}" />
                                                </c:url>
                                            </c:forEach>
                                        </div>
                                    </div>
                                    
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- section End -->
<!-- Quick-view modal popup start-->
<div class="modal fade bd-example-modal-lg theme-modal" id="quick-view" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
        <div class="modal-content quick-view-modal">
            <div class="modal-body">
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                <div class="row">
                    <div class="col-lg-6 col-xs-12">
                        <div class="quick-view-img"><img src="../assets/images/pro3/1.jpg" alt="" class="img-fluid blur-up lazyload"></div>
                    </div>
                    <div class="col-lg-6 rtl-text">
                        <div class="product-right">
                            <h2>Women Pink Shirt</h2>
                            <h3>$32.96</h3>
                            <ul class="color-variant">
                                <li class="bg-light0"></li>
                                <li class="bg-light1"></li>
                                <li class="bg-light2"></li>
                            </ul>
                            <div class="border-product">
                                <h6 class="product-title">product details</h6>
                                <p>Sed ut perspiciatis, unde omnis iste natus error sit voluptatem accusantium
                                    doloremque laudantium</p>
                            </div>
                            <div class="product-description border-product">
                                <div class="size-box">
                                    <ul>
                                        <li class="active"><a href="javascript:void(0)">s</a></li>
                                        <li><a href="javascript:void(0)">m</a></li>
                                        <li><a href="javascript:void(0)">l</a></li>
                                        <li><a href="javascript:void(0)">xl</a></li>
                                    </ul>
                                </div>
                                <h6 class="product-title">quantity</h6>
                                <div class="qty-box">
                                    <div class="input-group"><span class="input-group-prepend"><button type="button" class="btn quantity-left-minus" data-type="minus" data-field=""><i class="ti-angle-left"></i></button> </span>
                                        <input type="text" name="quantity" class="form-control input-number" value="1"> <span class="input-group-prepend"><button type="button" class="btn quantity-right-plus" data-type="plus" data-field=""><i class="ti-angle-right"></i></button></span>
                                    </div>
                                </div>
                            </div>
                            <div class="product-buttons"><a href="#" class="btn btn-solid">add to cart</a> <a href="#" class="btn btn-solid">view detail</a></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Quick-view modal popup end-->