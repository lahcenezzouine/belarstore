<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!-- section start -->
<section class="section-b-space ratio_asos">
    <div class="collection-wrapper">
        <div class="container">
            <div class="row">
                <div class="col-sm-3 collection-filter sidebar">
                    <!-- side-bar colleps block stat -->
                    <div class="collection-filter-block">
                        <!-- brand filter start -->
                        <div class="collection-mobile-back"><span class="filter-back"><i class="fa fa-angle-left" aria-hidden="true"></i> back</span></div>
                        <div class="collection-collapse-block open">
                            <h3 class="collapse-block-title">Nos Cat�gorie</h3>
                            <ul class="list-group list-group-flush">
                                <c:forEach var="category" items="${categories}">
                                    <li class="list-group-item "><a href="<c:url value='/store/${category.name.toLowerCase()}'/>">${category.name}</a></li>
                                </c:forEach> 
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-lg-9 col-sm-12 col-xs-12">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-xl-12">
                                <div class="filter-main-btn mb-2"><span class="filter-btn"><i class="fa fa-filter" aria-hidden="true"></i> filter</span></div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="product-slick">

                                    <c:forEach var="pi" items="${product.productImages}">
                                        <div  style="height: 300px;border:1px solid rgb(221 221 221)">
                                            <img src="http://old.belarome.ma/uploads/product_images/${pi.image.id}/thumb250_${pi.image.url}" alt="" 
                                                class="img-fluid blur-up lazyload bg-img-product image_zoom_cls-1" style="margin:auto;">
                                        </div>
                                    </c:forEach> 

                                </div>
                                <div class="row">
                                    <div class="col-12 p-0">
                                        <div class="slider-nav">
                                            <c:forEach var="pi" items="${product.productImages}">
                                                <div><img src="http://old.belarome.ma/uploads/product_images/${pi.image.id}/thumb250_${pi.image.url}" alt="" 
                                                          class="img-fluid blur-up lazyload" style="height: 150px; margin:auto;"></div>
                                            </c:forEach>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-6 rtl-text">
                                <div class="product-right">
                                    <h2>${product.name}</h2>
                                    <h3>${product.retailPrice} DH</h3>
                                    <!--<div class="product-buttons"><a href="#" data-toggle="modal" data-target="#addtocart" class="btn btn-solid">add to cart</a> <a href="#" class="btn btn-solid">buy now</a></div>-->
                                    <div class="border-product">
                                        <h6 class="product-title">D�tails</h6>
                                        <p>Sed ut perspiciatis, unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam eaque ipsa, quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt, explicabo. Nemo enim ipsam voluptatem,</p>
                                    </div>
                                    <div class="border-product">
                                        <h6 class="product-title">Partagez-le</h6>
                                        <div class="product-icon">
                                            <ul class="product-social">
                                                <li><a href="#" title="Facebook"><i class="fa fa-facebook"></i></a></li>
                                                <li><a href="#" title="Instagram"><i class="fa fa-instagram"></i></a></li>
                                                <li><a href="#" title="Email"><i class="fa fa-send"></i></a></li>
                                            </ul>
                                            <form class="d-inline-block">
                                                <button class="wishlist-btn"><i class="fa fa-heart"></i><span class="title-font">Ajouter � la liste de souhaits</span></button>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <br><br>
                    <!-- related products -->
                    <section class="section-b-space pt-0 ratio_asos">
                        <div class="container">
                            <div class="row">
                                <div class="col-12 product-related product-right">
                                    <h2>Produit dans la m�me cat�gorie</h2>
                                </div>
                            </div>
                            <div class="row search-product">
                                <c:forEach var="p" items="${productsInSameCategory}">
                                    <c:set var="image" value="${p.getMasterImage()}"/>
                                    <c:if test="${image != null && p.id != product.id}">
                                    <div class="col-md-2">
                                        <div class="product-box">
                                            <div class="img-wrapper">
                                                <div class="front">
                                                    <a href="<c:url value='/store/${product.getProductLink()}'/>" style="background-size: contain !important">
                                                        <img src="http://old.belarome.ma/uploads/product_images/${image.id}/thumb250_${image.url}"
                                                            class="img-fluid blur-up lazyload bg-img-product" alt="">
                                                    </a>
                                                </div>
                                                <div class="back">
                                                    <a href="<c:url value='/store/${p.getProductLink()}'/>">
                                                        <img src="http://old.belarome.ma/uploads/product_images/${image.id}/thumb250_${image.url}"
                                                            class="img-fluid blur-up lazyload bg-img-product" alt="">
                                                    </a>
                                                </div>
                                                <div class="cart-info cart-wrap">
                                                    <button data-toggle="modal" data-target="#addtocart"  title="Add to cart"><i class="ti-shopping-cart" ></i></button> <a href="javascript:void(0)" title="Add to Wishlist"><i class="ti-heart" aria-hidden="true"></i></a> <a href="#" data-toggle="modal" data-target="#quick-view" title="Quick View"><i class="ti-search" aria-hidden="true"></i></a> </div>
                                            </div>
                                            <div class="product-detail">
                                                <a href="<c:url value='/store/${p.getProductLink()}'/>">
                                                    <h6>${p.name}</h6></a>
                                                <h4>${p.retailPrice} DH</h4>
                                            </div>
                                        </div>
                                    </div>
                                    </c:if>
                                </c:forEach> 
                                
                            </div>
                        </div>
                    </section>
                    <!-- related products -->
                </div>
            </div>
        </div>
    </div>
</section>
<!-- section End -->