<div class="card">
    <div class="card-header">
        <div class="pull-right">
            <button type="button" class="btn btn-primary"
                    onclick="resetCategoryForm()"
                    data-toggle="modal" data-original-title="test" data-target="#addCategoryModal">
                <i class="fa fa-plus"></i> 
                Ajouter
            </button>     
        </div>
    </div>
    <div class="card-body">
        <div class="table-responsive">
            <table id="categories-table" class="table table-sm hover" style="width:100%">
                <thead>
                    <tr>
                        <th>Code</th>
                        <th>Nom</th>
                        <th>Visibilité</th>
                        <th></th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>
</div>

<!-- Modal -->
<div class="modal fade" tabindex="-1" id="addCategoryModal" role="dialog" aria-labelledby="addCategoryModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title f-w-600" id="addCategoryModalLabel">Nouvelle Catégorie</h5>
                <button class="close" type="button" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
            </div>
            <div class="modal-body">
                <form class="needs-validation">
                    <div class="form">
                        <div class="form-group">
                            <label for="category_code" class="mb-1">Code :</label>
                            <input class="form-control text-uppercase" id="category_code" type="text">
                        </div>
                        <div class="form-group">
                            <label for="category_name" class="mb-1">Nom :</label>
                            <input class="form-control text-uppercase" id="category_name" type="text">
                        </div>
                        <div class="form-group mb-0">
                            <label for="category_image" class="mb-1">Image :</label>
                            <input class="form-control" id="category_image" type="file">
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button class="btn btn-primary" type="button" onclick="createUpadteCategory()">
                    <i class="fa fa-save"></i> Valider
                </button>
                <button class="btn btn-link" type="button" data-dismiss="modal">Annuler</button>
            </div>
        </div>
    </div>
</div>