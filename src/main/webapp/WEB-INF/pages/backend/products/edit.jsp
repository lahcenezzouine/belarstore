<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<div class="card tab2-card">
    <div class="card-body ">
        <form:form method="POST" action="edit" modelAttribute="product" class="needs-validation">
            <form:hidden path="id"></form:hidden>
            <div class="form-group row">
                <form:label path="category.id" class="col-xl-3 col-md-4">Catégorie<span>*</span></form:label>
                <form:select path="category.id" items="${categories}" itemLabel="name" itemValue="id"
                             class="form-control col-xl-8 col-md-7">
                </form:select>
            </div>
            
            <div class="form-group row">
                <form:label path="name" class="col-xl-3 col-md-4">Nom<span>*</span></form:label>
                <form:input path="name" class="form-control col-xl-8 col-md-7"></form:input>
            </div>
            
            <div class="form-group row">
                <form:label path="retailPrice" class="col-xl-3 col-md-4">Prix<span>*</span></form:label>
                <form:input path="retailPrice" class="form-control col-xl-8 col-md-7"></form:input>
            </div>
            
            <div class="form-group row">
                <form:label path="topproduct" class="col-xl-3 col-md-4">Top Produit<span>*</span></form:label>
                <div class="col-xl-8 col-md-7">
                <form:checkbox path="topproduct" value="false"></form:checkbox>
                </div>
            </div>
            
            <div class="form-group row">
                <div class="col-xl-3 col-md-4"></div>
                <div class="col-xl-8 col-md-7">
                    <div class="pull-right" style="margin-right: -20px;">
                        <button type="submit" class="btn btn-primary">
                            <i class="fa fa-save"></i> Valider
                        </button>
                    </div>                
                </div>
            </div>
        </form:form>
    </div>
</div>
