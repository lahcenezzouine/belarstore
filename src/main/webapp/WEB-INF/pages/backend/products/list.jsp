<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<div class="card">
    <div class="card-header">
        <div class="pull-right">
            <a href="<c:url value='/admin/products/create'/>" class="btn btn-primary">
                <i class="fa fa-plus"></i> 
                Ajouter
            </a>     
        </div>
    </div>
    <div class="card-body">
        <div class="table-responsive">
            <table id="products-table" class="table table-sm hover" style="width:100%">
                <thead>
                    <tr>
                        <th>Nom</th>
                        <th>Prix</th>
                        <th>Catégorie</th>
                        <th>Top Produits</th>
                        <th>Visibilité</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    <c:forEach var="p" items="${products}">
                        <tr>
                            <td><a href="<c:url value='products/${p.id}/edit'/>"> ${p.name}</a></td>
                            <td>${p.retailPrice}</td>
                            <td>${p.category.name}</td>
                            <td>
                                <input type="checkbox" class="bootstrap-toggle" 
                                    onclick="updateProductTopProduct(this, ${p.id})"
                                    data-toggle="toggle" ${p.topproduct ? 'checked' : ''} >
                            </td>
                            <td>
                                <input type="checkbox" class="bootstrap-toggle" 
                                    onclick="updateProductVisibility(this, ${p.id})"
                                    data-toggle="toggle" ${p.visibility ? 'checked' : ''} >
                            </td>
                            <td>
                                <a href="#" onclick="deleteProduct(this, ${p.id})">
                                    <i class="fa fa-trash" aria-hidden="true"></i>
                                </a>
                            </td>
                        </tr>
                    </c:forEach>
                </tbody>
            </table>
        </div>
    </div>
</div>
