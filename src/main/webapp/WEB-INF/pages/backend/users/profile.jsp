<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<div class="row">
    <div class="col-xl-4">
        <div class="card">
            <div class="card-body">
                <div class="profile-details text-center">
                    <img src="<c:url value='/resources/images/Lhcnpic.jpg'/>" alt="" class="img-fluid img-90 rounded-circle blur-up lazyloaded">
                    <h5 class="f-w-600 mb-0">${user.firstName} ${user.lastName}</h5>
                    <span>${user.login}</span>
                    <div class="social">
                        <div class="form-group btn-showcase">
                            <button class="btn social-btn btn-fb d-inline-block"> <i class="fa fa-facebook"></i></button>
                            <button class="btn social-btn btn-twitter d-inline-block"><i class="fa fa-google"></i></button>
                            <button class="btn social-btn btn-google d-inline-block mr-0"><i class="fa fa-twitter"></i></button>
                        </div>
                    </div>
                </div>
                <hr>
            </div>
        </div>
    </div>
    <div class="col-xl-8">
        <div class="card tab2-card">
            <div class="card-body">
                <ul class="nav nav-tabs nav-material" id="top-tab" role="tablist">
                    <li class="nav-item"><a class="nav-link active" id="top-profile-tab" data-toggle="tab" href="#top-profile" role="tab" aria-controls="top-profile" aria-selected="true"><i data-feather="user" class="mr-2"></i>Profile</a>
                    </li>
                </ul>
                <div class="tab-content" id="top-tabContent">
                    <div class="tab-pane fade show active" id="top-profile" role="tabpanel" aria-labelledby="top-profile-tab">
                        <h5 class="f-w-600">Profile</h5>

                        <form:form method="POST" action="users/${user.id}/edit?fromProfile=true" modelAttribute="user" class="needs-validation">
                            <form:hidden path="id"></form:hidden>

                                <div class="form-group row">
                                <form:label path="login" class="col-xl-3 col-md-4">Email<span>*</span></form:label>
                                <form:input path="login" class="form-control col-xl-8 col-md-7"></form:input>
                                </div>
                                <div class="form-group row">
                                <form:label path="password" class="col-xl-3 col-md-4">Mote de passe<span>*</span></form:label>
                                <form:input path="password" type ="password" class="form-control col-xl-8 col-md-7"></form:input>
                                </div>


                                <div class="form-group row">
                                <form:label path="lastName" class="col-xl-3 col-md-4">Nom<span>*</span></form:label>
                                <form:input path="lastName" class="form-control col-xl-8 col-md-7"></form:input>
                                </div>

                                <div class="form-group row">
                                <form:label path="firstName" class="col-xl-3 col-md-4">Prenom<span>*</span></form:label>
                                <form:input path="firstName" class="form-control col-xl-8 col-md-7"></form:input>
                                </div>

                                <div class="form-group row">
                                    <div class="col-xl-3 col-md-4"></div>
                                    <div class="col-xl-8 col-md-7">
                                        <div class="pull-right" style="margin-right: -20px;">
                                            <button type="submit" class="btn btn-primary">
                                                <i class="fa fa-save"></i> Valider
                                            </button>
                                        </div>                
                                    </div>
                                </div>
                        </form:form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

