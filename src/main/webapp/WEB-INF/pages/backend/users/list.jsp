<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<div class="card">
    <div class="card-header">
        <div class="pull-right">
            <a href="<c:url value='/admin/users/create'/>" class="btn btn-primary">
                <i class="fa fa-plus"></i> 
                Ajouter
            </a>     
        </div>
    </div>
    <div class="card-body">
        <div class="table-responsive">
            <table id="users-table" class="table table-sm hover" style="width:100%">
                <thead>
                    <tr>
                        <th>Login</th>
                        <th>Nom</th>
                        <th>Prenom</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    <c:forEach var="u" items="${users}">
                        <tr>
                            <td><a href="<c:url value='users/${u.id}/edit'/>"> ${u.login}</a></td>
                            <td>${u.lastName}</td>
                            <td>${u.firstName}</td>
                            <td>
                                <a href="#" onclick="deleteUser(this, ${u.id})">
                                    <i class="fa fa-trash" aria-hidden="true"></i>
                                </a>
                            </td>
                        </tr>
                    </c:forEach>
                </tbody>
            </table>
        </div>
    </div>
</div>
