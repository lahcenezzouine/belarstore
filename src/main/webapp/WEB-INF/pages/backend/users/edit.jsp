<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<div class="card tab2-card">
    <div class="card-body ">
        <form:form method="POST" action="edit" modelAttribute="user" class="needs-validation">
            <form:hidden path="id"></form:hidden>

            <div class="form-group row">
                <form:label path="login" class="col-xl-3 col-md-4">Email<span>*</span></form:label>
                <form:input path="login" class="form-control col-xl-8 col-md-7"></form:input>
            </div>
            <div class="form-group row">
                <form:label path="password" class="col-xl-3 col-md-4">Mote de passe<span>*</span></form:label>
                <form:input path="password" type ="password" class="form-control col-xl-8 col-md-7"></form:input>
            </div>
            
            
            <div class="form-group row">
                <form:label path="lastName" class="col-xl-3 col-md-4">Nom<span>*</span></form:label>
                <form:input path="lastName" class="form-control col-xl-8 col-md-7"></form:input>
            </div>
            
            <div class="form-group row">
                <form:label path="firstName" class="col-xl-3 col-md-4">Prenom<span>*</span></form:label>
                <form:input path="firstName" class="form-control col-xl-8 col-md-7"></form:input>
            </div>
            
            <div class="form-group row">
                <div class="col-xl-3 col-md-4"></div>
                <div class="col-xl-8 col-md-7">
                    <div class="pull-right" style="margin-right: -20px;">
                        <button type="submit" class="btn btn-primary">
                            <i class="fa fa-save"></i> Valider
                        </button>
                    </div>                
                </div>
            </div>
        </form:form>
    </div>
</div>
