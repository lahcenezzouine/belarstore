<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<div class="card tab2-card">
    <div class="card-body ">
        <form:form method="POST" action="create" modelAttribute="page" class="needs-validation">
            <ul class="nav nav-tabs tab-coupon" id="myTab" role="tablist">
                <li class="nav-item"><a class="nav-link active show" id="general-tab" data-toggle="tab" href="#general" role="tab" aria-controls="general" aria-selected="true" data-original-title="" title="">G�n�ral</a></li>
                <li class="nav-item"><a class="nav-link" id="seo-tabs" data-toggle="tab" href="#seo" role="tab" aria-controls="seo" aria-selected="false" data-original-title="" title="">SEO</a></li>
            </ul>
            <div class="tab-content" id="myTabContent">
                <div class="tab-pane fade active show" id="general" role="tabpanel" aria-labelledby="general-tab">
                    <h4>General</h4>
                    <div class="form-group row">
                        <form:label path="code" class="col-xl-3 col-md-4">Lien<span>*</span></form:label>
                        <form:input path="code" class="form-control col-xl-8 col-md-7"></form:input>
                        </div>
                        <div class="form-group row">
                        <form:label path="name" class="col-xl-3 col-md-4">Nom<span>*</span></form:label>
                        <form:input path="name" class="form-control col-xl-8 col-md-7"></form:input>
                        </div>
                        <div class="form-group row">
                            <label class="col-xl-3 col-md-4">Status</label>
                            <div class="checkbox checkbox-primary col-xl-8 col-md-7">
                            <form:checkbox path="visibility" title="Activer la Page"/>
                            <label for="visibility1">Activer la Page</label>
                        </div>
                        <div class="form-group row editor-label">
                            <form:label path="htmlContent" class="col-xl-3 col-md-4">Description<span>*</span></form:label>
                            <div class="col-xl-8 col-md-7 editor-space">
                                <form:textarea path="htmlContent" cols="30" rows="10"></form:textarea>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="tab-pane fade" id="seo" role="tabpanel" aria-labelledby="seo-tabs">
                    <h4>SEO</h4>
                    <div class="form-group row">
                        <form:label path="metaTitle" class="col-xl-3 col-md-4">Meta Title</form:label>
                        <form:input path="metaTitle" class="form-control col-xl-8 col-md-7"></form:input>
                        </div>
                        <div class="form-group row">
                        <form:label path="metaDescription" class="col-xl-3 col-md-4">Meta Description</form:label>
                        <form:textarea path="metaDescription" class="form-control col-xl-8 col-md-7" cols="30" rows="10"></form:textarea>
                        </div>
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-xl-3 col-md-4"></div>
                    <div class="col-xl-8 col-md-7">
                        <div class="pull-right" style="margin-right: -20px;">
                            <button type="submit" class="btn btn-primary">
                                <i class="fa fa-save"></i> Valider
                            </button>
                        </div>                
                    </div>
                </div>
        </form:form>
    </div>
</div>
