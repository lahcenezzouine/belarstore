<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<div class="card">
    <div class="card-header">
        <div class="pull-right">
            <a href="<c:url value='/admin/pages/create'/>" class="btn btn-primary">
                <i class="fa fa-plus"></i> 
                Ajouter
            </a>     
        </div>
    </div>
    <div class="card-body">
        <div class="table-responsive">
            <table id="pages-table" class="table table-sm hover" style="width:100%">
                <thead>
                    <tr>
                        <th>Code</th>
                        <th>Nom</th>
                        <th>Visibilité</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    <c:forEach var="p" items="${pages}">
                        <tr>
                            <td>${p.code}</td>
                            <td><a href="<c:url value='pages/${p.id}/edit'/>"> ${p.name}</a></td>
                            <td>
                                <input type="checkbox" class="bootstrap-toggle" 
                                    onclick="updatePageVisibility(this, ${p.id})"
                                    data-toggle="toggle" ${p.visibility ? 'checked' : ''} >
                            </td>
                            <td>
                                <a href="#" onclick="deletePage(this, ${p.id})">
                                    <i class="fa fa-trash" aria-hidden="true"></i>
                                </a>
                            </td>
                        </tr>
                    </c:forEach>
                </tbody>
            </table>
        </div>
    </div>
</div>