<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page isELIgnored="false" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<tiles:importAttribute name="pages_javascripts" ignore="true"/>

<!DOCTYPE html>
<html lang="fr">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="author" content="pixelstrap">
        <link rel="icon" href="<c:url value='/resources/images/dashboard/favicon.png'/>" type="image/x-icon">
        <link rel="shortcut icon" href="<c:url value='/resources/images/dashboard/favicon.png'/>" type="image/x-icon">
        <title><tiles:getAsString name="backend.title" /> - Belarome</title>

        <!-- Google font-->
        <link href="https://fonts.googleapis.com/css?family=Work+Sans:100,200,300,400,500,600,700,800,900" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">

        <!-- Font Awesome-->
        <link rel="stylesheet" type="text/css" href="<c:url value='/resources/css/fontawesome.css'/>">

        <!-- Flag icon-->
        <link rel="stylesheet" type="text/css" href="<c:url value='/resources/css/flag-icon.css'/>">

        <!-- ico-font-->
        <link rel="stylesheet" type="text/css" href="<c:url value='/resources/css/icofont.css'/>">

        <!-- Prism css-->
        <link rel="stylesheet" type="text/css" href="<c:url value='/resources/css/prism.css'/>">

        <!-- Chartist css -->
        <link rel="stylesheet" type="text/css" href="<c:url value='/resources/css/chartist.css'/>">

        <!-- Bootstrap css-->
        <link rel="stylesheet" type="text/css" href="<c:url value='/resources/css/bootstrap.css'/>">

        <!-- datatables plugin -->
        <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.21/css/dataTables.bootstrap4.min.css">
        <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/responsive/2.2.4/js/dataTables.responsive.min.js">
        <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/responsive/2.2.4/js/responsive.bootstrap4.min.js">

        <!-- App css-->
        <link rel="stylesheet" type="text/css" href="<c:url value='/resources/css/admin.css'/>">
        <link rel="stylesheet" type="text/css" href="<c:url value='/resources/css/admin-belastore.css'/>">

        <link href="https://res.cloudinary.com/dxfq3iotg/raw/upload/v1557232134/toasty.css" rel="stylesheet" />
    </head>

    <body>
        <div class="page-wrapper">
            <header class="page-main-header">
                <tiles:insertAttribute name="backend.header" />
            </header>

            <section class="page-body-wrapper">
                <div class="page-sidebar">
                    <tiles:insertAttribute name="backend.sidebar" />
                </div>

                <!-- page-body Starts-->
                <div class="page-body">
                    <div class="container-fluid">        
                        <div class="page-header">
                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="page-header-left">
                                        <h3>
                                            <tiles:getAsString name="backend.title" />
                                            <small>Administration Belastore</small>
                                        </h3>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="container-fluid">
                        <tiles:insertAttribute name="backend.body" />
                    </div>
                </div>
                <!-- page-body Ends-->
                <footer>
                    <tiles:insertAttribute name="backend.footer" />
                </footer>
            </section>
        </div>

        <tiles:insertAttribute name="backend.scripts" />

        <!-- pages scripts-->
        <c:forEach var="script" items="${pages_javascripts}">
            <script src="<c:url value="${script}"/>"></script>
        </c:forEach> 
    </body>
</html>