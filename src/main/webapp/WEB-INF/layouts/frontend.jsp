<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page isELIgnored="false" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>

<!DOCTYPE html>
<html lang="fr">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width,initial-scale=1">
    <meta name="description" content="Belarôme, Produits naturel pour la santé et la beauté بيلاروم، منتجات طبيعية للصحة والجمال">
    <meta name="keywords" content="huile, huile essentielle, savon, savon noire, shampoing, belarome, belarôme, belarome casablanca, belarome fes, belarome maroc, belarôme maroc," />
    <meta property="fb:app_id" content="1828189497400109"/>
    <link rel="icon" href="<c:url value='/resources/images/bela.png'/>" type="image/x-icon">
    <link rel="shortcut icon" href="<c:url value='/resources/images/bela.png'/>" type="image/x-icon">
    <title><tiles:getAsString name="title" /> - Belarôme, Produits naturel pour la santé et la beauté</title>

   <!-- Google font-->
        <link href="https://fonts.googleapis.com/css?family=Lato:300,400,700,900" rel="stylesheet">

        <!-- Icons -->
        <link rel="stylesheet" type="text/css" href="<c:url value='/resources/css/fontawesome.css'/>">

        <!--Slick slider css-->
        <link rel="stylesheet" type="text/css" href="<c:url value='/resources/css/slick.css'/>">
        <link rel="stylesheet" type="text/css" href="<c:url value='/resources/css/slick-theme.css'/>">

        <!-- Animate icon -->
        <link rel="stylesheet" type="text/css" href="<c:url value='/resources/css/animate.css'/>">

        <!-- Themify icon -->
        <link rel="stylesheet" type="text/css" href="<c:url value='/resources/css/themify-icons.css'/>">

        <!-- Bootstrap css -->
        <link rel="stylesheet" type="text/css" href="<c:url value='/resources/css/bootstrap.css'/>">

        <!-- Theme css -->
        <link rel="stylesheet" type="text/css" href="<c:url value='/resources/css/theme.css'/>" media="screen" id="color">
        <link rel="stylesheet" type="text/css" href="<c:url value='/resources/css/belarome.css'/>" media="screen" id="color">

</head>

    <body>
        <header class="sticky">
            <tiles:insertAttribute name="header" />
        </header>

        <section>
            <tiles:insertAttribute name="body" />
        </section>

        <footer class="footer-light">
            <tiles:insertAttribute name="footer" />
        </footer>
        
        <tiles:insertAttribute name="script" />
    </body>
</html>