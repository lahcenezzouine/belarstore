<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!-- latest jquery-->
<script src="<c:url value='/resources/js/jquery-3.3.1.min.js'/>"></script>

<!-- menu js-->
<script src="<c:url value='/resources/js/menu.js'/>"></script>

<!-- lazyload js-->
<script src="<c:url value='/resources/js/lazysizes.min.js'/>"></script>

<!-- popper js-->
<script src="<c:url value='/resources/js/popper.min.js'/>"></script>

<!-- slick js-->
<script src="<c:url value='/resources/js/slick.js'/>"></script>

<!-- Bootstrap js-->
<script src="<c:url value='/resources/js/bootstrap.js'/>"></script>

<!-- Bootstrap Notification js-->
<script src="<c:url value='/resources/js/bootstrap-notify.min.js'/>"></script>

<!-- Theme js-->
<script src="<c:url value='/resources/js/script.js'/>"></script>

<script>
    $(window).on('load', function () {
        $('#exampleModal').modal('show');

    });

    function openSearch() {
        document.getElementById("search-overlay").style.display = "block";
    }

    function closeSearch() {
        document.getElementById("search-overlay").style.display = "none";
    }
</script>