<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<div class="mobile-fix-option">
        <div class="top-header">
            <div class="container">
                <div class="row">
                    <div class="col-lg-6">
                        <div class="header-contact">
                            <ul>
                                <li>Bienvenue dans notre magasin Belar�me</li>
                                <li>
                                    <i class="fa fa-phone" aria-hidden="true"></i>
                                    <a style="color:white" href="tel:+212678181863">+212 678 181 863</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
</div>     
<div class="container">
    <div class="row">
        <div class="col-sm-12">
            <div class="main-menu">
                <div class="menu-left"> 
                    <div class="brand-logo">
                        <a href="<c:url value='/'/>"><img style="height:50px" src="<c:url value='/resources/images/belarome-logo.png'/>" class="img-fluid blur-up lazyloaded" alt=""></a>
                    </div>
                </div>
                <div class="menu-right pull-right">
                    <div>
                        <nav id="main-nav">
                            <div class="toggle-nav"><i class="fa fa-bars sidebar-bar"></i></div>
                            <ul id="main-menu" class="sm pixelstrap sm-horizontal" data-smartmenus-id="16130414762900403">
                                <li>
                                    <div class="mobile-back text-right">
                                        <i class="fa fa-angle-right pl-2" aria-hidden="true"></i>
                                    </div>
                                </li>
                                <li>
                                    <a href="<c:url value='/'/>" class="has-submenu" id="sm-1581503694316498-1" aria-haspopup="true" aria-controls="sm-1581503694316498-2" aria-expanded="false">
                                        Accueil
                                    </a>
                                </li>
                                <li>
                                    <a href="<c:url value='/store'/>" class="has-submenu" id="sm-1581503694316498-1" aria-haspopup="true" aria-controls="sm-1581503694316498-2" aria-expanded="false">
                                        Boutique
                                    </a>
                                </li>
                                <li>
                                    <a href="<c:url value='/about'/>" class="has-submenu" id="sm-1581503694316498-1" aria-haspopup="true" aria-controls="sm-1581503694316498-2" aria-expanded="false">
                                        Qui somme nous?
                                    </a>
                                </li>
                                <li>
                                    <a href="#" class="has-submenu" id="sm-1581503694316498-1" aria-haspopup="true" aria-controls="sm-1581503694316498-2" aria-expanded="false">
                                        Nos magasins
                                    </a>
                                </li>
                                <li>
                                    <a href="#contact" class="has-submenu" id="sm-1581503694316498-1" aria-haspopup="true" aria-controls="sm-1581503694316498-2" aria-expanded="false">
                                        Contact
                                    </a>
                                </li>
                                <li>
                                    <a href="#" class="has-submenu" id="sm-1581503694316498-1" aria-haspopup="true" aria-controls="sm-1581503694316498-2" aria-expanded="false">
                                        Blog
                                    </a>
                                </li>
                            </ul>
                        </nav>
                    </div> 
                </div>
            </div>
        </div>
    </div>
</div>