<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<div class="light-layout">
    <div class="container">
        <section class="small-section border-section border-top-0">
            <div class="row">
                <div class="col-lg-6">
                    <div class="subscribe">
                        <div>
                            <h4>Abonnez-vous � notre newsletter</h4>
                            <p>Cadeaux exclusifs et superbes surprises de beaut� et sant�. </p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6">
                    <form action="https://pixelstrap.us19.list-manage.com/subscribe/post?u=5a128856334b598b395f1fc9b&amp;id=082f74cbda" class="form-inline subscribe-form auth-form needs-validation" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" target="_blank">
                        <div class="form-group mx-sm-3">
                            <input type="text" class="form-control" name="EMAIL" id="mce-EMAIL" placeholder="Entrer votre email" required="required">
                        </div>
                        <button type="submit" class="btn btn-solid" id="mc-submit">s'abonner</button>
                    </form>
                </div>
            </div>
        </section>
    </div>
</div>
<section class="section-b-space light-layout">
    <div class="container">
        <div class="row footer-theme partition-f">
            <div class="col-lg-4 col-md-6">
                <div class="footer-title footer-mobile-title">
                    <h4>about</h4>
                </div>
                <div class="footer-contant">
                    <div class="footer-logo"><img src="<c:url value='/resources/images/belarome-logo-white-d5ba8090b3b6b373be2b01dfc00fb2aa.png'/>" alt=""></div>
                    <p>Belar�me est une entreprise sp�cialis�e dans la production de produits naturels pour la sant� et la beaut�.

                    </p>
                    <div class="footer-social">
                        <ul>
                            <li><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                            <li><a href="#"><i class="fa fa-google-plus" aria-hidden="true"></i></a></li>
                            <li><a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                            <li><a href="#"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
                            <li><a href="#"><i class="fa fa-rss" aria-hidden="true"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col offset-xl-1">
                <div class="sub-title">
                    <div class="footer-title">
                        <h4>Liens</h4>
                    </div>
                    <div class="footer-contant">
                        <ul>
                            <li><a href="#">Nos Cat�gories</a></li>
                            <li><a href="#">Nos Produits</a></li>
                            <li><a href="#">Points ventes</a></li>
                            <li><a href="about_Belarome.html">Qui Somme Nous</a></li>
                        </ul>
                    </div>
                </div>
            </div>

            <div class="col">
                <div class="sub-title">
                    <div class="footer-title">
                        <h4>Contact</h4>
                    </div>
                    <div class="footer-contant">
                        <ul class="contact-list">

                            <li style="text-transform:uppercase">
                                <i class="fa fa-map-marker"></i> 13 Jenane Nahra Sidi Boujida, Bab Elkhoukha, F�s - Maroc
                            </li>
                            <li><i class="fa fa-phone"></i>+212 535 762 330</li>
                            <li><i class="fa fa-phone"></i>+212 678 181 863</li>
                            <li><i class="fa fa-envelope-o"></i><a href="mailto:contact@belarome.ma">contact@belarome.ma</a></li>

                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>