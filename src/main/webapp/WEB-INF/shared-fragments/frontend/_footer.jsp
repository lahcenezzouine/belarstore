<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<div class="light-layout">
    <div class="container">
        <section class="small-section border-section border-top-0">
            
        </section>
    </div>
</div>
<section class="section-b-space light-layout">
    <div class="container">
        <div class="row footer-theme partition-f">
            <div class="col-lg-4 col-md-6">
                <div class="footer-title footer-mobile-title">
                    <h4>Enreprise</h4>
                </div>
                <div class="footer-contant" id="about-us">
                    <div class="footer-logo"><img src="<c:url value='/resources/images/belarome-logo-white-d5ba8090b3b6b373be2b01dfc00fb2aa.png'/>" alt=""></div>
                    <p>
                        Belar�me est une entreprise sp�cialis�e dans la production de produits naturels pour la sant� et la beaut�.
                    </p>
                    <div class="footer-social">
                        <ul>
                            <li><a href="https://fb.me/belaromemaroc" target="_blank"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                            <li><a href="#"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
                            <li><a href="https://www.pinterest.com/BelaromeMaroc/" target="_blank"><i class="fa fa-pinterest" aria-hidden="true"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col offset-xl-1">
                <div class="sub-title">
                    <div class="footer-title">
                        <h4>Liens</h4>
                    </div>
                    <div class="footer-contant">
                        <ul>
                            <li><a href="<c:url value='/'/>">Accueil</a></li>
                            <li><a href="<c:url value='/store'/>">Boutique</a></li>
                            <li><a href="<c:url value='/about'/>">Qui Somme Nous</a></li>
                            <li><a href="#">Nos magasins</a></li>
                            <li><a href="#">Contact</a></li>
                            <li><a href="#">Blog</a></li>
                        </ul>
                    </div>
                </div>
            </div>

            <div class="col">
                <div class="sub-title">
                    <div class="footer-title" id="contact">
                        <h4>Contact</h4>
                    </div>
                    <div class="footer-contant">
                        <ul class="contact-list">

                            <li style="text-transform:uppercase">
                                <i class="fa fa-map-marker"></i> MAG N�3 E2 68 LOT AL JAWAHER RTE BENSOUDA ZOUAGHA F�s - Maroc
                            </li>
                            <li><i class="fa fa-phone"></i>+212 678 181 863</li>
                            <li><i class="fa fa-envelope-o"></i><a href="mailto:contact@belarome.ma">contact@belarome.ma</a></li>

                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>