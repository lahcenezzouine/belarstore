<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!-- latest jquery-->
<script src="<c:url value='/resources/js/jquery-3.3.1.min.js'/>"></script>

<!-- Bootstrap js-->
<script src="<c:url value='/resources/js/popper.min.js'/>"></script>
<script src="<c:url value='/resources/js/bootstrap.js'/>"></script>

<!-- feather icon js-->
<script src="<c:url value='/resources/js/icons/feather-icon/feather.min.js'/>"></script>
<script src="<c:url value='/resources/js/icons/feather-icon/feather-icon.js'/>"></script>

<!-- Sidebar jquery-->
<script src="<c:url value='/resources/js/sidebar-menu.js'/>"></script>

<!--chartist js-->
<script src="<c:url value='/resources/js/chart/chartist/chartist.js'/>"></script>

<!--chartjs js-->
<script src="<c:url value='/resources/js/chart/chartjs/chart.min.js'/>"></script>

<!-- lazyload js-->
<script src="<c:url value='/resources/js/lazysizes.min.js'/>"></script>

<!-- datatables plugin -->
<script src="https://cdn.datatables.net/1.10.21/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.21/js/dataTables.bootstrap4.min.js"></script>

<!--copycode js-->
<script src="<c:url value='/resources/js/prism/prism.min.js'/>"></script>
<script src="<c:url value='/resources/js/clipboard/clipboard.min.js'/>"></script>
<script src="<c:url value='/resources/js/custom-card/custom-card.js'/>"></script>

<!--counter js-->
<script src="<c:url value='/resources/js/counter/jquery.waypoints.min.js'/>"></script>
<script src="<c:url value='/resources/js/counter/jquery.counterup.min.js'/>"></script>
<script src="<c:url value='/resources/js/counter/counter-custom.js'/>"></script>

<!--peity chart js-->
<script src="<c:url value='/resources/js/chart/peity-chart/peity.jquery.js'/>"></script>

<!--sparkline chart js-->
<script src="<c:url value='/resources/js/chart/sparkline/sparkline.js'/>"></script>

<!--Customizer admin-->
<script src="<c:url value='/resources/js/admin-customizer.js'/>"></script>

<!--right sidebar js-->
<script src="<c:url value='/resources/js/chat-menu.js'/>"></script>

<!--height equal js-->
<script src="<c:url value='/resources/js/height-equal.js'/>"></script>

<!-- lazyload js-->
<script src="<c:url value='/resources/js/lazysizes.min.js'/>"></script>

<!-- Toasty plugin-->
<script src="https://res.cloudinary.com/dxfq3iotg/raw/upload/v1557232134/toasty.js"></script>
<script src="<c:url value='/resources/js/toasty-with-sound.js'/>"></script>

<!--script admin-->
<script src="<c:url value='/resources/js/admin-script.js'/>"></script>
