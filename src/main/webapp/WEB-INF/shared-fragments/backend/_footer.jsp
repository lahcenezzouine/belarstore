<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<div class="container-fluid">
    <div class="row">
        <div class="col-md-6 footer-copyright">
            <p class="mb-0">Copyright 2020 � Belarome - Tous les droits sont r�serv�s.</p>
        </div>
        <div class="col-md-6">
            <p class="pull-right mb-0"><i class="fa fa-heart"></i></p>
        </div>
    </div>
</div>