<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<div class="main-header-left d-none d-lg-block">
    <div class="logo-wrapper"><a href="<c:url value='/'/>"><img class="blur-up lazyloaded" src="<c:url value='/resources/images/belarome-logo.png'/>" style="height: 60px;" alt=""></a></div>
</div>
<div class="sidebar custom-scrollbar">
    <ul class="sidebar-menu">
        <li><a class="sidebar-header" href="<c:url value='/admin'/>"><i data-feather="home"></i><span>Dashboard</span></a></li>
        <li><a class="sidebar-header" href="<c:url value='/admin/products'/>"><i data-feather="box"></i> <span>Produits</span><i class="fa fa-angle-right pull-right"></i></a>
            <ul class="sidebar-submenu">
                <li>
                    <li><a href="<c:url value='/admin/products'/>"><i class="fa fa-circle"></i>Liste</a></li>
                    <li><a href="<c:url value='/admin/products/create'/>"><i class="fa fa-circle"></i>Nouveau</a></li>
                </li>
            </ul>
        </li>
        <li><a class="sidebar-header" href="<c:url value='/admin/categories'/>"><i data-feather="grid"></i> <span>Catégories</span></a>
        </li>
        <li><a class="sidebar-header" href="#"><i data-feather="clipboard"></i><span>Pages</span><i class="fa fa-angle-right pull-right"></i></a>
            <ul class="sidebar-submenu">
                <li><a href="<c:url value='/admin/pages'/>"><i class="fa fa-circle"></i>List Page</a></li>
                <li><a href="<c:url value='/admin/pages/create'/>"><i class="fa fa-circle"></i>Create Page</a></li>
            </ul>
        </li>
        <li><a class="sidebar-header" href="/admin/users"><i data-feather="user-plus"></i><span>Utilisateur</span><i class="fa fa-angle-right pull-right"></i></a>
            <ul class="sidebar-submenu">
                <li><a href="<c:url value='/admin/users'/>"><i class="fa fa-circle"></i>Liste</a></li>
                <li><a href="<c:url value='/admin/users/create'/>"><i class="fa fa-circle"></i>Nouveau</a></li>
            </ul>
        </li>
        <li><a class="sidebar-header" href=""><i data-feather="settings" ></i><span>Paramétres</span><i class="fa fa-angle-right pull-right"></i></a>
            <ul class="sidebar-submenu">
                <li><a href="<c:url value='/admin/profile'/>"><i class="fa fa-circle"></i>Profil</a></li>
            </ul>
        </li>
    </ul>
</div>