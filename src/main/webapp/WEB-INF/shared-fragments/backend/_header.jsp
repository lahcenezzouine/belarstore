<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<div class="main-header-right row">
    <div class="main-header-left d-lg-none">
        <div class="logo-wrapper"><a href="index.html"><img class="blur-up lazyloaded" src="<c:url value='/resources/images/belarome-logo.png'/>" alt=""></a></div>
    </div>
    <div class="mobile-sidebar">
        <div class="media-body text-right switch-sm">
            <label class="switch"><a href="#"><i id="sidebar-toggle" data-feather="align-left"></i></a></label>
        </div>
    </div>
    <div class="nav-right col">
        <ul class="nav-menus">
            <li><a class="text-dark" target="_blank" title="Boutique" href="<c:url value='/'/>"><i data-feather="shopping-cart"></i></a></li>
            <li><a class="text-dark" href="#!" onclick="javascript:toggleFullScreen()"><i data-feather="maximize-2"></i></a></li>
            <li class="onhover-dropdown">
                <div class="media align-items-center"><img class="align-self-center pull-right img-50 rounded-circle blur-up lazyloaded" src="<c:url value='/resources/images/Lhcnpic.jpg'/>" alt="header-user">
                    <div class="dotted-animation"><span class="animate-circle"></span><span class="main-circle"></span></div>
                </div>
                <ul class="profile-dropdown onhover-show-div p-20 profile-dropdown-hover">
                    <li><a href="<c:url value='/admin/profile'/>"><i data-feather="user"></i>Profil</a></li>
                    <!--<li><a href="#"><i data-feather="lock"></i>Lock Screen</a></li>-->
                    <!--<li><a href="#"><i data-feather="settings"></i>Paramètres</a></li>-->
                    <li><a href="<c:url value='/logout'/>"><i data-feather="log-out"></i>Logout</a></li>
                </ul>
            </li>
        </ul>
        <div class="d-lg-none mobile-toggle pull-right"><i data-feather="more-horizontal"></i></div>
    </div>
</div>