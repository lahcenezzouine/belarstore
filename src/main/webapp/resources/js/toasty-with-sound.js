$(document).ready(function () {
    var options =
            {
                autoClose: true,
                progressBar: true,
                enableSounds: true,
                sounds: {
                    // path to sound for info message:
                    info: "https://res.cloudinary.com/dxfq3iotg/video/upload/v1557233294/info.mp3",
                    // path to sound for successfull message:
                    success: "https://res.cloudinary.com/dxfq3iotg/video/upload/v1557233524/success.mp3",
                    // path to sound for warn message:
                    warning: "https://res.cloudinary.com/dxfq3iotg/video/upload/v1557233563/warning.mp3",
                    // path to sound for error message:
                    error: "https://res.cloudinary.com/dxfq3iotg/video/upload/v1557233574/error.mp3"
                }
            };
    window.AppToaster = new Toasty(options);
    AppToaster.configure(options);
    // Usage
    //AppToaster.success("This toast notification with sound");
    //AppToaster.info("This toast notification with sound");
    //AppToaster.warning("This toast notification with sound");
    //AppToaster.error("This toast notification with sound");
});