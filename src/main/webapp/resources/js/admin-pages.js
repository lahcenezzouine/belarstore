/*==============================================================
 >> admin/pages page functions
 ==========================*/
$(document).ready(function () {

    // Init CKeditor
    if($('#htmlContent').length > 0) {
        CKEDITOR.replace('htmlContent', {
            on: {
                contentDom: function (evt) {
                    // Allow custom context menu only with table elemnts.
                    evt.editor.editable().on('contextmenu', function (contextEvent) {
                        var path = evt.editor.elementPath();

                        if (!path.contains('table')) {
                            contextEvent.cancel();
                        }
                    }, null, null, 5);
                }
            }
        });
    };

    window.pagesDatatable = $('#pages-table')
            .DataTable({
                language: {url: 'https://cdn.datatables.net/plug-ins/1.10.21/i18n/French.json'},
                columnDefs: [
                    {
                        orderDataType: "dom-checkbox-sort",
                        targets: 2
                    }
                ]
            });
});

var deletePage = function (element, pageId) {
    let result = confirm('Êtes-vous sûr de vouloir supprimer?');
    if (result === true) {
        $.ajax({
            url: 'pages/delete',
            type: 'POST',
            dataType: 'json',
            contentType: 'application/json; charset=utf-8',
            data: JSON.stringify({id: pageId}),
            success: function (result) {
                if (result === true) {
                    $(element).parents('tr').remove();
                    AppToaster.success('Opération réssit.');
                } else {
                    AppToaster.error("Une erreur s'est produite lors de la traitement de votre demande.");
                }
            },
            error: function () {
                AppToaster.error("Une erreur s'est produite lors de la traitement de votre demande.");
            }
        });
    }
};

var updatePageVisibility = function (element, pageId) {
    $.ajax({
        url: 'pages/' + pageId + '/updateVisibility?isVisible=' + element.checked,
        type: 'POST',
        dataType: 'json',
        success: function (result) {
            if (result === true) {
                AppToaster.success('Mise à jour effectuer avec succès.');
            } else {
                AppToaster.error("Une erreur s'est produite lors de la traitement de votre demande.");
            }
        },
        error: function () {
            AppToaster.error("Une erreur s'est produite lors de la traitement de votre demande.");
        }
    });
};