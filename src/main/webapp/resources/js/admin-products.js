/*==============================================================
 >> admin/products product functions
 ==========================*/
$(document).ready(function () {

    window.productsDatatable = $('#products-table')
            .DataTable({
                language: {url: 'https://cdn.datatables.net/plug-ins/1.10.21/i18n/French.json'},
                columnDefs: [
                    {
                        orderDataType: "dom-checkbox-sort",
                        targets: 3
                    }
                ]
            });
            
});

var deleteProduct = function (element, productId) {
    let result = confirm('Êtes-vous sûr de vouloir supprimer?');
    if (result === true) {
        $.ajax({
            url: 'products/delete',
            type: 'POST',
            dataType: 'json',
            contentType: 'application/json; charset=utf-8',
            data: JSON.stringify({id: productId}),
            success: function (result) {
                if (result === true) {
                    $(element).parents('tr').remove();
                    AppToaster.success('Opération réssit.');
                } else {
                    AppToaster.error("Une erreur s'est produite lors de la traitement de votre demande.");
                }
            },
            error: function () {
                AppToaster.error("Une erreur s'est produite lors de la traitement de votre demande.");
            }
        });
    }
};

var updateProductVisibility = function (element, productId) {
    $.ajax({
        url: 'products/' + productId + '/updateVisibility?isVisible=' + element.checked,
        type: 'POST',
        dataType: 'json',
        success: function (result) {
            if (result === true) {
                AppToaster.success('Mise à jour effectuer avec succès.');
            } else {
                AppToaster.error("Une erreur s'est produite lors de la traitement de votre demande.");
            }
        },
        error: function () {
            AppToaster.error("Une erreur s'est produite lors de la traitement de votre demande.");
        }
    });
};
var updateProductTopProduct = function (element, productId) {
    $.ajax({
        url: 'products/' + productId + '/updateTopProduct?isTopProduct=' + element.checked,
        type: 'POST',
        dataType: 'json',
        success: function (result) {
            if (result === true) {
                AppToaster.success('Mise à jour effectuer avec succès.');
            } else {
                AppToaster.error("Une erreur s'est produite lors de la traitement de votre demande.");
            }
        },
        error: function () {
            AppToaster.error("Une erreur s'est produite lors de la traitement de votre demande.");
        }
    });
};