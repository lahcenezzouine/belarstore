/*==============================================================
 >> admin/users user functions
 ==========================*/
$(document).ready(function () {

    window.usersDatatable = $('#users-table')
            .DataTable({ language: {url: 'https://cdn.datatables.net/plug-ins/1.10.21/i18n/French.json' }});
});

var deleteUser = function (element, userId) {
    let result = confirm('Êtes-vous sûr de vouloir supprimer?');
    if (result === true) {
        $.ajax({
            url: 'users/delete',
            type: 'POST',
            dataType: 'json',
            contentType: 'application/json; charset=utf-8',
            data: JSON.stringify({id: userId}),
            success: function (result) {
                if (result === true) {
                    $(element).parents('tr').remove();
                    AppToaster.success('Opération réssit.');
                } else {
                    AppToaster.error("Une erreur s'est produite lors de la traitement de votre demande.");
                }
            },
            error: function () {
                AppToaster.error("Une erreur s'est produite lors de la traitement de votre demande.");
            }
        });
    }
};


