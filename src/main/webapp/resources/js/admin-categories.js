
/*==============================================================
 >> admin/categories page functions
 ==========================*/
window.editedCategory = {};
var updateCategoryVisibility = function (element, categoryId) {
    $.ajax({
        url: 'categories/' + categoryId + '/updateVisibility?isVisible=' + element.checked,
        type: 'POST',
        dataType: 'json',
        success: function (result) {
            if (result === true) {
                AppToaster.success('Mise à jour effectuer avec succès.');
            } else {
                AppToaster.error("Une erreur s'est produite lors de la traitement de votre demande.");
            }
        },
        error: function () {
            AppToaster.error("Une erreur s'est produite lors de la traitement de votre demande.");
        }
    });
};
var createUpadteCategory = function () {
    var data = {
        id: window.editedCategory.categoryId,
        code: $('#category_code').val(),
        name: $('#category_name').val()
    };
    var operationUrl = data.id ? 'categories/update' : 'categories/create';
    $.ajax({
        url: operationUrl,
        type: 'POST',
        dataType: 'json',
        contentType: 'application/json; charset=utf-8',
        data: JSON.stringify(data),
        success: function (result) {
            if (result === true) {
                window.categoryDatatable.ajax.reload();
                AppToaster.success('Opération réssit.');
            } else {
                AppToaster.error("Une erreur s'est produite lors de la traitement de votre demande.");
            }
            $('#addCategoryModal').modal('hide');
        },
        error: function () {
            AppToaster.error("Une erreur s'est produite lors de la traitement de votre demande.");
        }
    });
};
var deleteCategory = function (categoryId) {
    let result = confirm('Êtes-vous sûr de vouloir supprimer?');
    if (result === true) {
        $.ajax({
            url: 'categories/delete',
            type: 'POST',
            dataType: 'json',
            contentType: 'application/json; charset=utf-8',
            data: JSON.stringify({id: categoryId}),
            success: function (result) {
                if (result === true) {
                    window.categoryDatatable.ajax.reload();
                    AppToaster.success('Opération réssit.');
                } else {
                    AppToaster.error("Une erreur s'est produite lors de la traitement de votre demande.");
                }
            },
            error: function () {
                AppToaster.error("Une erreur s'est produite lors de la traitement de votre demande.");
            }
        });
    }

};

var resetCategoryForm = function () {
    window.editedCategory = {};
    $('#category_code').val('');
    $('#category_name').val('');
};

var showCategoryModal = function (element, categoryId) {
    resetCategoryForm();
    window.editedCategory.categoryId = categoryId;
    window.editedCategory.isCreateMode = false;
    $('#category_code').val($(element).data('category-code'));
    $('#category_name').val($(element).data('category-name'));
    $('#addCategoryModal').modal('show');
};

$(document).ready(function () {
    // categories page
    window.categoryDatatable = $('#categories-table')
            .DataTable({
                language: {
                    url: 'https://cdn.datatables.net/plug-ins/1.10.21/i18n/French.json'
                },
                ajax: {
                    url: 'categories/findAll',
                    dataSrc: ''
                },
                columns: [
                    {data: 'code'},
                    {data: 'name'},
                    {
                        width: '10%',
                        data: 'id',
                        className: 'dt-body-center',
                        render: function (data, type, row, meta) {
                            let checked = row.visibility ? 'checked' : '';
                            return `<input type="checkbox" class="bootstrap-toggle" 
                                    onclick="updateCategoryVisibility(this, ` + row.id + `)"
                                    data-toggle="toggle"` + checked + `>`;
                        }
                    },
                    {
                        width: '6%',
                        orderable: false,
                        data: 'id',
                        className: 'dt-body-center',
                        render: function (data, type, row, meta) {
                            return `<a href="#" 
                                    data-category-code="` + row.code + `"
                                    data-category-name="` + row.name + `"
                                    onclick="showCategoryModal(this, ` + row.id + `)">
                                <i class="fa fa-pencil-square-o" aria-hidden="true""></i></a>
                                <a href="#" 
                                    data-category-code="` + row.code + `"
                                    data-category-name="` + row.name + `"
                                    onclick="deleteCategory(` + row.id + `)">
                                <i class="fa fa-trash" aria-hidden="true""></i></a>`;
                        }
                    }
                ]
            });

});