package ma.belarome.belastore.dto;
// Generated Mar 8, 2020 2:49:16 PM by Hibernate Tools 4.3.1

import ma.belarome.belastore.entities.*;
import com.fasterxml.jackson.annotation.JsonIgnore;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.IDENTITY;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import lombok.*;

@Getter
@Setter
public class ProductDto implements java.io.Serializable {

    private Integer id;
    private Category category;
    private String name;
    private BigDecimal retailPrice;
    private Boolean visibility;
    private Boolean topproduct;
    private List<ProductImage> productImages = new ArrayList<ProductImage>();

    public String getProductLink() {
        return category.getName().toLowerCase().concat("/").concat(this.getName().toLowerCase());
    }

    public Image getMasterImage() {
        return productImages.size() > 0 ? productImages.get(0).getImage() : null;
    }
}
