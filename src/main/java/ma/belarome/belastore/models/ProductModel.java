/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ma.belarome.belastore.models;

import ma.belarome.belastore.entities.Product;

/**
 *
 * @author lahcen.ezzouine
 */
public class ProductModel extends AbstractModel<Product> {
    
    public ProductModel() {
        super(Product.class);
    }
}
