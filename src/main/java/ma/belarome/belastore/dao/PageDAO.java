/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ma.belarome.belastore.dao;

import org.hibernate.*;
import java.io.*;
import java.util.*;
import ma.belarome.belastore.entities.*;
import ma.belarome.belastore.models.HibernateUtil;
import org.hibernate.criterion.Restrictions;

/**
 *
 * @author lahcen.ezzouine
 */
public class PageDAO {

    protected SessionFactory sessionFactory = HibernateUtil.getSessionFactory();

    public PageDAO() {
    }

    public Page find(Object id) {
        Page result = null;
        Session session = null;
        Transaction transaction = null;
        try {
            session = sessionFactory.openSession();
            transaction = session.beginTransaction();
            result = (Page) session.get(Page.class, (Serializable) id);
            transaction.commit();
        } catch (Exception e) {
            result = null;
            if (transaction != null) {
                transaction.rollback();
            }
        } finally {
            session.close();
        }
        return result;
    }

    public List<Page> findAll() {
        List<Page> result = null;
        Session session = sessionFactory.openSession();
        try {
            Criteria cr = session.createCriteria(Page.class);

            result = cr.list();
        } catch (Exception e) {
            result = null;
        } finally {
            session.close();
        }
        return result;
    }

    public List<Page> findAllVisible() {
        List<Page> result = null;
        Session session = sessionFactory.openSession();
        try {
            Criteria cr = session.createCriteria(Page.class);
            cr.add(Restrictions.like("visibility", true));
            result = cr.list();
        } catch (Exception e) {
            result = null;
        } finally {
            session.close();
        }
        return result;
    }

    public boolean create(Page entity) {
        boolean result = true;
        Session session = null;
        Transaction transaction = null;
        try {
            session = sessionFactory.openSession();
            transaction = session.beginTransaction();
            session.save(entity);
            transaction.commit();
        } catch (Exception e) {
            result = false;
            if (transaction != null) {
                transaction.rollback();
            }
        } finally {
            session.close();
        }
        return result;
    }
    
    public boolean update(Page entity) {
        boolean result = true;
        Session session = null;
        Transaction transaction = null;
        try {
            session = sessionFactory.openSession();
            transaction = session.beginTransaction();
            session.update(entity);
            transaction.commit();
        } catch (Exception e) {
            result = false;
            if (transaction != null) {
                transaction.rollback();
            }
        } finally {
            session.close();
        }
        return result;
    }

    public boolean delete(Page entity) {
        boolean result = true;
        Session session = null;
        Transaction transaction = null;
        try {
            session = sessionFactory.openSession();
            transaction = session.beginTransaction();
            session.delete(entity);
            transaction.commit();
        } catch (Exception e) {
            result = false;
            if (transaction != null) {
                transaction.rollback();
            }
        } finally {
            session.close();
        }
        return result;
    }
    

    public Boolean updateVisibility(int id, boolean isVisible) {
        boolean result = true;
        Session session = null;
        Transaction transaction = null;
        try {
            session = sessionFactory.openSession();
            transaction = session.beginTransaction();
            Page category = (Page) session.get(Page.class, (Serializable) id);
            category.setVisibility(isVisible);
            transaction.commit();
        } catch (Exception e) {
            result = false;
            if (transaction != null) {
                transaction.rollback();
            }
        } finally {
            session.close();
        }
        return result;
    }
}
