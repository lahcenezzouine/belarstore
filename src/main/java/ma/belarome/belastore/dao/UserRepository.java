/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ma.belarome.belastore.dao;

import ma.belarome.belastore.entities.*;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

/**
 *
 * @author lahcen.ezzouine
 */
@Repository
public interface UserRepository extends CrudRepository<User, Integer> {
    @Query("from User u where u.login=:userName")
    User selectByUserName(@Param("userName") String userName);
}