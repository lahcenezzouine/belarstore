/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ma.belarome.belastore.dao;

import java.util.List;
import ma.belarome.belastore.entities.*;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

/**
 *
 * @author lahcen.ezzouine
 */
@Repository
public interface ProductRepository extends CrudRepository<Product, Integer> {

    List<Product> findByName(String name);
    
    @Query("SELECT p FROM Product p WHERE p.category.id IN (SELECT c.id FROM Category c WHERE c.name = :categoryName)")
    List<Product> findByCategoryName(@Param("categoryName") String categoryName);
    
    @Query("SELECT p FROM Product p WHERE p.visibility = true")
    List<Product> selectVisibleProducts();
        
    @Query("SELECT p FROM Product p WHERE p.topproduct = true AND p.visibility = true")
    List<Product> selectTopProducts();
}
