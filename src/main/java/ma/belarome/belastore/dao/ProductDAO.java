/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ma.belarome.belastore.dao;

import org.hibernate.*;
import java.io.*;
import java.util.*;
import ma.belarome.belastore.entities.*;
import ma.belarome.belastore.models.HibernateUtil;
import org.hibernate.criterion.Restrictions;

/**
 *
 * @author lahcen.ezzouine
 */
public class ProductDAO {

    protected SessionFactory sessionFactory = HibernateUtil.getSessionFactory();

    public ProductDAO() {
    }

    public Product find(Object id) {
        Product result = null;
        Session session = null;
        Transaction transaction = null;
        try {
            session = sessionFactory.openSession();
            transaction = session.beginTransaction();
            result = (Product) session.get(Product.class, (Serializable) id);
            transaction.commit();
        } catch (Exception e) {
            result = null;
            if (transaction != null) {
                transaction.rollback();
            }
        } finally {
            session.close();
        }
        return result;
    }

    public List<Product> findAll() {
        List<Product> result = null;
        Session session = sessionFactory.openSession();
        try {
            Criteria cr = session.createCriteria(Product.class);

            result = cr.list();
        } catch (Exception e) {
            result = null;
        } finally {
            session.close();
        }
        return result;
    }

    public List<Product> findAllVisible() {
        List<Product> result = null;
        Session session = sessionFactory.openSession();
        try {
            Criteria cr = session.createCriteria(Product.class);
            cr.add(Restrictions.like("visibility", true));
            result = cr.list();
        } catch (Exception e) {
            result = null;
        } finally {
            session.close();
        }
        return result;
    }

    public boolean create(Product entity) {
        boolean result = true;
        Session session = null;
        Transaction transaction = null;
        try {
            session = sessionFactory.openSession();
            transaction = session.beginTransaction();
            session.save(entity);
            transaction.commit();
        } catch (Exception e) {
            result = false;
            if (transaction != null) {
                transaction.rollback();
            }
        } finally {
            session.close();
        }
        return result;
    }

    public boolean update(Product entity) {
        boolean result = true;
        Session session = null;
        Transaction transaction = null;
        try {
            session = sessionFactory.openSession();
            transaction = session.beginTransaction();
            session.update(entity);
            transaction.commit();
        } catch (Exception e) {
            result = false;
            if (transaction != null) {
                transaction.rollback();
            }
        } finally {
            session.close();
        }
        return result;
    }

    public boolean delete(Product entity) {
        boolean result = true;
        Session session = null;
        Transaction transaction = null;
        try {
            session = sessionFactory.openSession();
            transaction = session.beginTransaction();
            session.delete(entity);
            transaction.commit();
        } catch (Exception e) {
            result = false;
            if (transaction != null) {
                transaction.rollback();
            }
        } finally {
            session.close();
        }
        return result;
    }

    public Boolean updateVisibility(int id, boolean isVisible) {
        boolean result = true;
        Session session = null;
        Transaction transaction = null;
        try {
            session = sessionFactory.openSession();
            transaction = session.beginTransaction();
            Product category = (Product) session.get(Product.class, (Serializable) id);
            category.setVisibility(isVisible);
            transaction.commit();
        } catch (Exception e) {
            result = false;
            if (transaction != null) {
                transaction.rollback();
            }
        } finally {
            session.close();
        }
        return result;
    }

    public Boolean updateTopProduct(int id, boolean isTopProduct) {
        boolean result = true;
        Session session = null;
        Transaction transaction = null;
        try {
            session = sessionFactory.openSession();
            transaction = session.beginTransaction();
            Product product = (Product) session.get(Product.class, (Serializable) id);
            product.setTopproduct(isTopProduct);
            transaction.commit();
        } catch (Exception e) {
            result = false;
            if (transaction != null) {
                transaction.rollback();
            }
        } finally {
            session.close();
        }
        return result;
    }

    public List<Product> findAllByCategory(String categoryName) {
        List<Product> result = null;
        Session session = sessionFactory.openSession();
        try {
            Criteria cr = session.createCriteria(Product.class);

            cr.createAlias("category", "cat");
            cr.add(Restrictions.like("cat.name", categoryName));
            result = cr.list();
        } catch (Exception e) {
            result = null;
        } finally {
            session.close();
        }
        return result;
    }

    public List<Product> getTopProducts() {
        List<Product> result = null;
        Session session = sessionFactory.openSession();
        try {
            Criteria cr = session.createCriteria(Product.class);
            cr.add(Restrictions.like("topproduct", true));
            result = cr.list();
        } catch (Exception e) {
            result = null;
        } finally {
            session.close();
        }
        return result;
    }
}
