/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ma.belarome.belastore.dao;

import java.util.List;
import ma.belarome.belastore.entities.*;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

/**
 *
 * @author lahcen.ezzouine
 */
@Repository
public interface CategoryRepository extends CrudRepository<Category, Integer> {
    
    List<Category> findByName(String name);

    @Query("SELECT c FROM Category c WHERE c.visibility = true")
    List<Category> selectVisibleCategories();
}