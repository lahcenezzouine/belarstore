/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ma.belarome.belastore.dao;

import java.util.List;
import ma.belarome.belastore.entities.*;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

/**
 *
 * @author lahcen.ezzouine
 */
@Repository
public interface PageRepository extends CrudRepository<Page, Integer> {

    List<Page> findByName(String name);
}