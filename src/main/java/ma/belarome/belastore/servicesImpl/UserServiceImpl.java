/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ma.belarome.belastore.servicesImpl;

import java.util.List;
import ma.belarome.belastore.dao.UserRepository;
import ma.belarome.belastore.entities.User;
import ma.belarome.belastore.services.IUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

/**
 *
 * @author lahcen.ezzouine
 */
@Service
public class UserServiceImpl implements IUserService {

    @Autowired
    private UserRepository userRepository;

    @Override
    public User insert(User newObj) {
        // encrypt password
        newObj.setPassword(new BCryptPasswordEncoder().encode(newObj.getPassword()));
        return userRepository.save(newObj);
    }

    @Override
    public User update(User newObj) {
        User old = userRepository.findById(newObj.getId()).get();
        old.setFirstName(newObj.getFirstName());
        old.setPassword(new BCryptPasswordEncoder().encode(newObj.getPassword()));
        return userRepository.save(old);
    }

    @Override
    public void delete(int id) {
        userRepository.deleteById(id);
    }

    @Override
    public User selectOne(int id) {
        return userRepository.findById(id).get();
    }

    @Override
    public List<User> selectAll() {
        return (List<User>) userRepository.findAll();
    }

    @Override
    public User selectByUserName(String userName) {
        return userRepository.selectByUserName(userName);
    }
}