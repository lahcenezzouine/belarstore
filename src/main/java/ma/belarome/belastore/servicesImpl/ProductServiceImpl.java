/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ma.belarome.belastore.servicesImpl;

import java.util.List;
import java.util.stream.Collectors;
import ma.belarome.belastore.dao.ProductRepository;
import ma.belarome.belastore.dto.ProductDto;
import ma.belarome.belastore.entities.Product;
import ma.belarome.belastore.services.IProductService;
import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author lahcen.ezzouine
 */
@Service
public class ProductServiceImpl implements IProductService {

    @Autowired
    private ProductRepository productRepository;

    @Override
    public Product insert(Product newObj) {
        return productRepository.save(newObj);
    }

    @Override
    public Product update(Product newObj) {
        Product old = productRepository.findById(newObj.getId()).get();
        old.setName(newObj.getName());
        return productRepository.save(old);
    }

    @Override
    public boolean updateVisibility(int id, boolean isVisible) {
        boolean result = true;
        
        try {
            Product old = productRepository.findById(id).get();
            old.setVisibility(isVisible);
            productRepository.save(old);
        } catch (Exception e) {
            result = false;
        }
        
        return result;
    }

    @Override
    public boolean updateTopProduct(int id, boolean isTopProduct) {
        boolean result = true;
        
        try {
            Product old = productRepository.findById(id).get();
            old.setTopproduct(isTopProduct);
            productRepository.save(old);
        } catch (Exception e) {
            result = false;
        }
        
        return result;
    }

    @Override
    public void delete(int id) {
        productRepository.deleteById(id);
    }

    @Override
    public Product selectOne(int id) {
        return productRepository.findById(id).get();
    }

    @Override
    public List<ProductDto> selectAll() {
        List<Product> products = (List<Product>) productRepository.findAll();

        return mapProductsToDtos(products);
    }

    @Override
    public List<Product> selectByName(String name) {
        return productRepository.findByName(name);
    }

    @Override
    public ProductDto selectOneByName(String name) {
        ModelMapper modelMapper = new ModelMapper();
        return modelMapper.map(productRepository.findByName(name).get(0), ProductDto.class);
    }

    @Override
    public List<ProductDto> selectByCategoryName(String categoryName) {
        List<Product> products = (List<Product>) productRepository.findByCategoryName(categoryName);
        return mapProductsToDtos(products);
    }

    @Override
    public List<ProductDto> selectVisibleProducts() {
        List<Product> products = (List<Product>) productRepository.selectVisibleProducts();
        return mapProductsToDtos(products);
    }

    @Override
    public List<ProductDto> selectTopProducts() {
        List<Product> products = (List<Product>) productRepository.selectTopProducts();
        return mapProductsToDtos(products);
    }

    private List<ProductDto> mapProductsToDtos(List<Product> products) {
        // mapping Entity to DTO
        ModelMapper modelMapper = new ModelMapper();
        List<ProductDto> dtos = modelMapper.map(products,
                new TypeToken<List<ProductDto>>() {
                }.getType());

        return dtos;
    }
}