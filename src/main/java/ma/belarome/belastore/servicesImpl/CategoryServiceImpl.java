/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ma.belarome.belastore.servicesImpl;

import java.util.List;
import ma.belarome.belastore.dao.CategoryRepository;
import ma.belarome.belastore.entities.Category;
import ma.belarome.belastore.services.ICategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author lahcen.ezzouine
 */
@Service
public class CategoryServiceImpl implements ICategoryService {

    @Autowired
    private CategoryRepository categoryRepository;

    @Override
    public Category insert(Category newObj) {
        return categoryRepository.save(newObj);
    }

    @Override
    public Category update(Category newObj) {
        Category old = categoryRepository.findById(newObj.getId()).get();
        old.setName(newObj.getName());
        return categoryRepository.save(old);
    }

    @Override
    public Category updateVisibility(int id, Boolean isVisible) {
        Category old = categoryRepository.findById(id).get();
        old.setVisibility(isVisible);
        return categoryRepository.save(old);
    }

    @Override
    public void delete(int id) {
        categoryRepository.deleteById(id);
    }

    @Override
    public Category selectOne(int id) {
        return categoryRepository.findById(id).get();
    }

    @Override
    public List<Category> selectByName(String name) {
        return categoryRepository.findByName(name);
    }

    @Override
    public List<Category> selectAll() {
        return (List<Category>) categoryRepository.findAll();
    }

    @Override
    public List<Category> selectVisibleCategories() {
        return (List<Category>) categoryRepository.selectVisibleCategories();
    }
}