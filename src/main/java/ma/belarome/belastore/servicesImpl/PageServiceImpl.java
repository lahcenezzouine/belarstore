/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ma.belarome.belastore.servicesImpl;

import java.util.List;
import ma.belarome.belastore.dao.PageRepository;
import ma.belarome.belastore.entities.Page;
import ma.belarome.belastore.services.IPageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author lahcen.ezzouine
 */
@Service
public class PageServiceImpl implements IPageService {

    @Autowired
    private PageRepository categoryRepository;

    @Override
    public Page insert(Page newObj) {
        return categoryRepository.save(newObj);
    }

    @Override
    public Page update(Page newObj) {
        Page old = categoryRepository.findById(newObj.getId()).get();
        old.setName(newObj.getName());
        return categoryRepository.save(old);
    }

    @Override
    public Page updateVisibility(int id, Boolean isVisible) {
        Page old = categoryRepository.findById(id).get();
        old.setVisibility(isVisible);
        return categoryRepository.save(old);
    }

    @Override
    public void delete(int id) {
        categoryRepository.deleteById(id);
    }

    @Override
    public Page selectOne(int id) {
        return categoryRepository.findById(id).get();
    }

    @Override
    public List<Page> selectByName(String name) {
        return categoryRepository.findByName(name);
    }

    @Override
    public List<Page> selectAll() {
        return (List<Page>) categoryRepository.findAll();
    }
}