/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ma.belarome.belastore.controllers;

import java.util.List;
import ma.belarome.belastore.dto.ProductDto;
import ma.belarome.belastore.entities.*;
import ma.belarome.belastore.services.ICategoryService;
import ma.belarome.belastore.services.IProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/")
public class HomeController {

    @Autowired
    private ICategoryService categoryService;
    @Autowired
    private IProductService productService;

    @GetMapping("/")
    public String index(Model model) {
        List<ProductDto> topProducts = productService.selectTopProducts();
        List<Category> categories = categoryService.selectVisibleCategories();
        model.addAttribute("topProducts", topProducts);
        model.addAttribute("categories", categories);
        
        return "home.index";
    }

    @GetMapping("/about")
    public String about() {
        return "home.about";
    }

    @GetMapping("/store")
    public String store(Model model) {

        List<ProductDto> products = productService.selectAll();
        List<Category> categories = categoryService.selectAll();
        model.addAttribute("products", products);
        model.addAttribute("categories", categories);
        
        return "home.store";
    }

    @GetMapping("/store/{categoryName}")
    public String productsByCategory(@PathVariable("categoryName") String categoryName, Model model) {
        List<ProductDto> productsByCategory = productService.selectByCategoryName(categoryName);

        List<Category> categories = categoryService.selectVisibleCategories();
        model.addAttribute("products", productsByCategory);
        model.addAttribute("categories", categories);
        
        return "home.store";
    }

    @GetMapping("/store/{categoryName}/{productName}")
    public String productDetails(@PathVariable("categoryName") String categoryName,
            @PathVariable("productName") String productName, Model model) {

        ProductDto product = productService.selectOneByName(productName);
        List<ProductDto> productsInSameCategory = productService.selectByCategoryName(categoryName);
        List<Category> categories = categoryService.selectAll();

        model.addAttribute("product", product);
        model.addAttribute("categories", categories);
        model.addAttribute("productsInSameCategory", productsInSameCategory);
        
        return "home.store.product-details";
    }
}
