/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ma.belarome.belastore.controllers.admin;

import java.util.List;
import ma.belarome.belastore.config.ApplicationUser;
import ma.belarome.belastore.dao.*;
import ma.belarome.belastore.entities.*;
import ma.belarome.belastore.services.IUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

@Controller
@RequestMapping("/admin/users")
public class UserController {

    @Autowired
    private IUserService userService;

    @GetMapping("")
    public String index(Model model) {
        model.addAttribute("users", userService.selectAll());
        return "backend.users.list";
    }

    @GetMapping("/create")
    public String createGet(Model model) {
        model.addAttribute(new User());
        return "backend.users.create";
    }

    @PostMapping("/create")
    public String createPost(@ModelAttribute("user") User user, BindingResult result) {
        userService.insert(user);
        return "redirect:/admin/users";
    }

    @GetMapping("/{id}/edit")
    public String editGet(@PathVariable(name = "id") int id, Model model) {
        User user = userService.selectOne(id);
        model.addAttribute(user);
        return "backend.users.edit";
    }

    @PostMapping("/{id}/edit")
    public String editPost(@ModelAttribute("user") User user,
            @RequestParam(value = "error", required = false) Boolean fromProfile,
            BindingResult result) {
        userService.update(user);

        if (fromProfile != null && fromProfile) {
            return "redirect:/admin/profile";
        } else {
            return "redirect:/admin/users";
        }
    }

    @PostMapping("/delete")
    public @ResponseBody
    void delete(@RequestBody User model) {
        userService.delete(model.getId());
    }

//    @PostMapping("/{id}/updateVisibility")
//    public @ResponseBody
//    User updateVisibility(
//            @PathVariable(name = "id") int id,
//            @RequestParam(name = "isVisible") boolean isVisible) {
//        return userService.updateVisibility(id, isVisible);
//    }
}
