/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ma.belarome.belastore.controllers.admin;

import java.util.List;
import ma.belarome.belastore.dao.*;
import ma.belarome.belastore.entities.*;
import ma.belarome.belastore.services.ICategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

@Controller
@RequestMapping("/admin/categories")
public class CategoryController {

    @Autowired
    private ICategoryService categoryService;

    @GetMapping("")
    public String index() {
        return "backend.categories.list";
    }

    @GetMapping("/findAll")
    public @ResponseBody
    List<Category> categoriesJson() {
        List<Category> categories = categoryService.selectAll();
        return categories; // returns json array.
    }
    
    @PostMapping("/create")
    public @ResponseBody Category create(@RequestBody Category model) {
        Category result = categoryService.insert(model);
        return result;
    }
    
    @PostMapping("/update")
    public @ResponseBody Category update(@RequestBody Category model) {
        Category result = categoryService.update(model);
        return result;
    }
    
    @PostMapping("/delete")
    public @ResponseBody void delete(@RequestBody Category model) {
        categoryService.delete(model.getId());
    }

    @PostMapping("/{id}/updateVisibility")
    public @ResponseBody
    Category updateVisibility(
            @PathVariable(name = "id") int id,
            @RequestParam(name = "isVisible") boolean isVisible) {
        return categoryService.updateVisibility(id, isVisible);
    }
}