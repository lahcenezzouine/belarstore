/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ma.belarome.belastore.controllers.admin;

import java.util.List;
import ma.belarome.belastore.dao.*;
import ma.belarome.belastore.entities.*;
import ma.belarome.belastore.services.IPageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

@Controller
@RequestMapping("/admin/pages")
public class PageController {

    @Autowired
    private IPageService pageService;

    ;

    @GetMapping("")
    public String index(Model model) {
        model.addAttribute("pages", pageService.selectAll());
        return "backend.pages.list";
    }

    @GetMapping("/create")
    public String createGet(Model model) {
        model.addAttribute(new Page());
        return "backend.pages.create";
    }

    @PostMapping("/create")
    public String createPost(@ModelAttribute("page") Page page, BindingResult result) {
        pageService.insert(page);
        return "redirect:/admin/pages";
    }

    @GetMapping("/{id}/edit")
    public String editGet(@PathVariable(name = "id") int id, Model model) {
        Page page = pageService.selectOne(id);
        model.addAttribute(page);
        return "backend.pages.edit";
    }

    @PostMapping("/{id}/edit")
    public String editPost(@ModelAttribute("page") Page page, BindingResult result) {
        pageService.update(page);
        return "redirect:/admin/pages";
    }

    @PostMapping("/delete")
    public @ResponseBody
    void delete(@RequestBody Page model) {
        pageService.delete(model.getId());
    }

    @PostMapping("/{id}/updateVisibility")
    public @ResponseBody
    Page updateVisibility(
            @PathVariable(name = "id") int id,
            @RequestParam(name = "isVisible") boolean isVisible) {
        return pageService.updateVisibility(id, isVisible);
    }
}
