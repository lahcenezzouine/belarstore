    /*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ma.belarome.belastore.controllers.admin;

import ma.belarome.belastore.controllers.*;
import java.util.List;
import ma.belarome.belastore.dao.*;
import ma.belarome.belastore.entities.*;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;


@Controller
@RequestMapping("/admin")
public class DashboardController {

    @GetMapping("")
    public String dashbord(){
        return "backend.dashboard";
    }
}