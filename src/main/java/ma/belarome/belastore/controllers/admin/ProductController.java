/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ma.belarome.belastore.controllers.admin;

import java.util.List;
import ma.belarome.belastore.dao.*;
import ma.belarome.belastore.entities.*;
import ma.belarome.belastore.services.ICategoryService;
import ma.belarome.belastore.services.IProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

@Controller
@RequestMapping("/admin/products")
public class ProductController {

    @Autowired
    private IProductService productService;
    @Autowired
    private ICategoryService categoryService;
    
    @GetMapping("")
    public String index(Model model) {
        model.addAttribute("products", productService.selectAll());
        return "backend.products.list";
    }

    @GetMapping("/create")
    public String createGet(Model model) {
        model.addAttribute(new Product());
        model.addAttribute("categories", categoryService.selectAll());
        return "backend.products.create";
    }

    @PostMapping("/create")
    public String createPost(@ModelAttribute("product") Product product, BindingResult result) {
        productService.insert(product);
        return "redirect:/admin/products";  
    }

    @GetMapping("/{id}/edit")
    public String editGet(@PathVariable(name = "id") int id, Model model) {
        Product product = productService.selectOne(id);
        model.addAttribute(product);
        model.addAttribute("categories", categoryService.selectAll());
        return "backend.products.edit";
    }

    @PostMapping("/{id}/edit")
    public String editPost(@ModelAttribute("product") Product product, BindingResult result) {
        productService.update(product);
        return "redirect:/admin/products";  
    }

    @PostMapping("/delete")
    public @ResponseBody
    void delete(@RequestBody Product model) {
        productService.delete(model.getId());
    }

    @PostMapping("/{id}/updateVisibility")
    public @ResponseBody
    Boolean updateVisibility(
            @PathVariable(name = "id") int id,
            @RequestParam(name = "isVisible") boolean isVisible) {
        return productService.updateVisibility(id, isVisible);
    }
    
    @PostMapping("/{id}/updateTopProduct")
    public @ResponseBody
    Boolean updateTopProduct(
            @PathVariable(name = "id") int id,
            @RequestParam(name = "isTopProduct") boolean isTopProduct) {
        return productService.updateTopProduct(id, isTopProduct);
    }
}
