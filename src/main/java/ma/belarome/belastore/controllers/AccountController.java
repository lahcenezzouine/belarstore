/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ma.belarome.belastore.controllers;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import ma.belarome.belastore.config.ApplicationUser;
import ma.belarome.belastore.entities.*;
import ma.belarome.belastore.services.IUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

@Controller
public class AccountController {

    @Autowired
    private IUserService userService;
    
    @GetMapping("/login")
    public String loginGet(
            Model model,
            @RequestParam(value = "error", required = false) Boolean error) {
        if (error != null && error) {
            model.addAttribute("errorMsg", "Email ou Mot de passe est invalid!");
        }
        return "login";
    }

    @PostMapping("/login")
    public void loginPost(HttpServletRequest request, HttpServletResponse response)
            throws IOException, ServletException {
        request.getRequestDispatcher("/login").forward(request, response);
    }

    @GetMapping("/admin/profile")
    public String profile(Model model) {
        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();

        if (principal instanceof ApplicationUser) {
            User connectedUser = ((ApplicationUser) principal).getUserDetails();
            User user = userService.selectOne(connectedUser.getId());
            model.addAttribute(user);
            return "backend.users.profile";
        } else {
            return "redirect:/admin";
        }
    }
}
