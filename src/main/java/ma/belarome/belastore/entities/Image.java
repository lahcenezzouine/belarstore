package ma.belarome.belastore.entities;
// Generated Mar 8, 2020 2:49:16 PM by Hibernate Tools 4.3.1


import java.util.HashSet;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.IDENTITY;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * Image generated by hbm2java
 */
@Entity
@Table(name="image"
    ,catalog="belarome_store"
)
public class Image  implements java.io.Serializable {


     private Integer id;
     private String url;
     private String contentType;
     private String fileSize;
     private Set<ProductImage> productImages = new HashSet<ProductImage>(0);

    public Image() {
    }

    public Image(String url, String contentType, String fileSize, Set<ProductImage> productImages) {
       this.url = url;
       this.contentType = contentType;
       this.fileSize = fileSize;
       this.productImages = productImages;
    }
   
     @Id @GeneratedValue(strategy=IDENTITY)

    
    @Column(name="id", unique=true, nullable=false)
    public Integer getId() {
        return this.id;
    }
    
    public void setId(Integer id) {
        this.id = id;
    }

    
    @Column(name="url")
    public String getUrl() {
        return this.url;
    }
    
    public void setUrl(String url) {
        this.url = url;
    }

    
    @Column(name="content_type")
    public String getContentType() {
        return this.contentType;
    }
    
    public void setContentType(String contentType) {
        this.contentType = contentType;
    }

    
    @Column(name="file_size")
    public String getFileSize() {
        return this.fileSize;
    }
    
    public void setFileSize(String fileSize) {
        this.fileSize = fileSize;
    }

@OneToMany(fetch=FetchType.LAZY, mappedBy="image")
    public Set<ProductImage> getProductImages() {
        return this.productImages;
    }
    
    public void setProductImages(Set<ProductImage> productImages) {
        this.productImages = productImages;
    }




}


