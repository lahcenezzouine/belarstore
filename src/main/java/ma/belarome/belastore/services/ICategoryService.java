/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ma.belarome.belastore.services;

import java.util.List;
import ma.belarome.belastore.entities.Category;

/**
 *
 * @author lahcen.ezzouine
 */
public interface ICategoryService {
    
    Category insert(Category c);

    Category update(Category c);
    
    Category updateVisibility(int id, Boolean isVisible);

    void delete(int id);

    Category selectOne(int id);

    List<Category> selectByName(String name);

    List<Category> selectAll();
    
    List<Category> selectVisibleCategories();
}
