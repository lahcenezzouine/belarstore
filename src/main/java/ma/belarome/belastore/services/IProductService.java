/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ma.belarome.belastore.services;

import java.util.List;
import ma.belarome.belastore.dto.ProductDto;
import ma.belarome.belastore.entities.Product;

/**
 *
 * @author lahcen.ezzouine
 */
public interface IProductService {

    Product insert(Product c);

    Product update(Product c);

    boolean updateVisibility(int id, boolean isVisible);

    boolean updateTopProduct(int id, boolean isTopProduct);
    
    void delete(int id);

    Product selectOne(int id);
    
    List<ProductDto> selectAll();

    List<Product> selectByName(String name);

    ProductDto selectOneByName(String name);

    List<ProductDto> selectByCategoryName(String name);

    List<ProductDto> selectVisibleProducts();

    List<ProductDto> selectTopProducts();
}
