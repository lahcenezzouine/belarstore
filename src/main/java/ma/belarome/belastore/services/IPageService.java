/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ma.belarome.belastore.services;

import java.util.List;
import ma.belarome.belastore.entities.Page;

/**
 *
 * @author lahcen.ezzouine
 */
public interface IPageService {
    
    Page insert(Page c);

    Page update(Page c);

    Page updateVisibility(int id, Boolean isVisible);
    
    void delete(int id);

    Page selectOne(int id);

    List<Page> selectByName(String name);

    List<Page> selectAll();
}
