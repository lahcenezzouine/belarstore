/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ma.belarome.belastore.services;

import java.util.List;
import ma.belarome.belastore.entities.User;

/**
 *
 * @author lahcen.ezzouine
 */
public interface IUserService {

    User insert(User c);

    User update(User c);

    void delete(int id);

    User selectOne(int id);

    List<User> selectAll();
    
    User selectByUserName(String userName);
}
